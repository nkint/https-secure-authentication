#!/bin/sh

echo "===================================================="
echo "Create CA certificate, self sign and of course test:"
echo "===================================================="

openssl genrsa -des3 -out ca.key 2048
openssl req -new -x509 -days 365 -key ca.key -out ca.crt
openssl x509 -in ca.crt -text -noout

echo "===================================================="
echo "Create server certificate, request signing, sign with our CA and test:"
echo "===================================================="

openssl genrsa -out server.key 1024
openssl req -new -key server.key -out server.csr
openssl x509 -req -in server.csr -out server.crt -CA ca.crt -CAkey ca.key -CAcreateserial -days 365
openssl x509 -in server.crt -text -noout

echo "===================================================="
echo "Create userA certificate, request signing, sign with our CA and test:"
echo "===================================================="

openssl genrsa -out userA.key 1024
openssl req -new -key userA.key -out userA.csr
openssl x509 -req -in userA.csr -out userA.crt -CA ca.crt -CAkey ca.key -CAcreateserial -days 365
openssl x509 -in userA.crt -text -noout

echo "===================================================="
echo "Generating browser-required p12 certificate:"
echo "FYI:    To have this option working, you'll need to do:"
echo "FYI:    Go to appA/https_only_from_appB"
echo "FYI:    And set rejectUnauthorized: false in transport.js"
echo "===================================================="

openssl pkcs12 -export -in userA.crt -inkey userA.key -name "Eugene HP userA p12 certificate" -out userA.p12
open userA.p12















### OLD WAY

# echo "Create the CA Key and Certificate for signing Client Certs"
# openssl genrsa -des3 -out ca.key 4096
# openssl req -new -x509 -days 365 -key ca.key -out ca.crt

# echo "Create the Server Key, CSR, and Certificate"
# openssl genrsa -des3 -out server.key 1024
# openssl req -new -key server.key -out server.csr

# echo "We're self signing our own server cert here.  This is a no-no in production."
# #openssl x509 -req -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt
# openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

# echo "Create the Client Key and CSR"
# openssl genrsa -des3 -out client.key 1024
# openssl req -new -key client.key -out client.csr

# echo "Sign the client certificate with our CA cert.  Unlike signing our own server cert, this is what we want to do."
# openssl x509 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt

