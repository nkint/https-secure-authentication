var express = require('express'),
        app = express();

[
    'api.js',
    'transport.js'
].map (function(controllerName) {
    controller = require('./' + controllerName);
    controller.setup(app)
});